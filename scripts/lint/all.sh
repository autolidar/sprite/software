#!/usr/bin/env bash

source scripts/utils.sh --prefix "$0"

prefix_run scripts/lint/cmake.sh "$@"
if [ "$?" != "0" ]; then
  echo -e "\n ${BOLD}${BRED}---> Format error detected! Run \"scripts/lint/all.sh --fix\" to fix it. <---${NC}"
fi

prefix_run scripts/lint/python.sh "$@"

prefix_run scripts/lint/other.sh "$@"
