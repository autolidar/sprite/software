#!/usr/bin/env bash

source scripts/utils.sh --prefix "$0"

fix=

while [ "$1" != "" ]; do
  case $1 in
  --fix | -f)
    fix=1
    ;;
  esac
  shift
done

if [ "$fix" == "1" ]; then
  prefix_run prettier --write README.md "scripts/**/*.*" "src/**/*.*"
else
  prefix_run prettier --check README.md "scripts/**/*.*" "src/**/*.*"
fi
