#!/usr/bin/env bash

export NC='\e[0m' # No Color
export BOLD='\e[1m'

# Colors
export BLK='\e[30m'
export RED='\e[31m'
export GRN='\e[32m'
export YEL='\e[33m'
export BLU='\e[34m'
export MAG='\e[35m'
export CYA='\e[36m'
export WHT='\e[37m'

# Bright colors
export BBLK='\e[90m'
export BRED='\e[91m'
export BGRN='\e[92m'
export BYEL='\e[93m'
export BBLU='\e[94m'
export BMAG='\e[95m'
export BCYA='\e[96m'
export BWHT='\e[97m'

# Background colors
export BLKB='\e[40m'
export REDB='\e[41m'
export GRNB='\e[42m'
export YELB='\e[43m'
export BLUB='\e[44m'
export MAGB='\e[45m'
export CYAB='\e[46m'
export WHTB='\e[47m'

# Bright background colors
export BBLKB='\e[100m'
export BREDB='\e[101m'
export BGRNB='\e[102m'
export BYELB='\e[103m'
export BBLUB='\e[104m'
export BMAGB='\e[105m'
export BCYAB='\e[106m'
export BWHTB='\e[107m'

prefix=

while [ "$1" != "" ]; do
  case $1 in
  --prefix)
    prefix=$2
    shift
    ;;
  esac
  shift
done

prefix_print() {
  echo -e "\n${GRN}[$prefix]${NC} $*"
}

prefix_run() {
  prefix_print "Running '$*'..."
  "$@"
}

update_apt_cache() {
  # Wrapper for "apt-get update" that only runs it once, and caches the list of upgradable packages
  if [ -z "${APT_CACHE_UPDATED}" ]; then
    prefix_run sudo apt-get update
    if [ ! -f /usr/lib/update-notifier/apt-check ]; then
      prefix_run sudo apt-get install -qq update-notifier-common
    fi
    UPGRADABLE_PACKAGES=$(/usr/lib/update-notifier/apt-check -p 2>&1)
    APT_CACHE_UPDATED=1
    export UPGRADABLE_PACKAGES APT_CACHE_UPDATED
  fi
}

apt_install() {
  # Wrapper for "apt-get install" that only passes the packages that need install or upgrade
  update_apt_cache
  packages=()

  for package in "$@"; do
    INSTALL_STATE=$(dpkg -l | grep -c "^ii.* $package .*$" || true) # 0 == package is not installed
    UPGRADE_STATE=$(echo "$UPGRADABLE_PACKAGES" | grep -c "^$package$" || true) # 0 == no upgrade necessary
    if [ "$INSTALL_STATE" == "0" ] || [ "$UPGRADE_STATE" != "0" ]; then
      packages+=("$package")
    fi
  done

  if [ ${#packages[@]} -ne 0 ]; then
    prefix_run sudo DEBIAN_FRONTEND=noninteractive apt-get install -y "${packages[@]}"
    unset APT_CACHE_UPDATED
  fi
}

update_pip_cache() {
  # Wrapper for "python3 -m pip list --outdated" that only runs it once, and caches it
  if [ -z "${PIP_CACHE_UPDATED}" ]; then
    prefix_run sudo -H python3 -m pip install -U pip
    OUTDATED_MODULES=$(python3 -m pip list --outdated)
    PIP_CACHE_UPDATED=1
    export OUTDATED_MODULES PIP_CACHE_UPDATED
  fi
}

pip_install() {
  # Wrapper for "python3 -m pip install -U" that only passes the modules that need install or upgrade
  update_pip_cache
  modules=()

  for module in "$@"; do
    INSTALL_STATE=$(python3 -m pip freeze | grep -c "^$module==.*$" || true)  # 0 == module is not installed
    UPGRADE_STATE=$(echo "$OUTDATED_MODULES" | grep -c "^$module .*$" || true)  # 0 == no upgrade necessary
    if [ "$INSTALL_STATE" == "0" ] || [ "$UPGRADE_STATE" != "0" ]; then
      modules+=("$module")
    fi
  done

  if [ ${#modules[@]} -ne 0 ]; then
    prefix_run sudo -H python3 -m pip install -U "${modules[@]}"
  fi
}
