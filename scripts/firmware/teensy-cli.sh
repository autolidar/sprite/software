#!/usr/bin/env bash

set -e

# SHOULD ALWAYS BE RUN FROM PROJECT ROOT
TEENSY_LOADER_PATH=arduino-ide/hardware/tools/teensy.exe
TEENSY_LOADER_CLI_PATH=$(command -pv teensy_loader_cli)

if ! command -v wslpath > /dev/null; then
  echo "Detected Linux, calling teensy_loader_cli directly..."
  exec $TEENSY_LOADER_CLI_PATH --mcu=TEENSY35 -w -v "$1"
  exit
else
  echo "Detected WSL, working magic..."
fi

HEX_PATH=$(realpath "$1")
HEX_DIR=$(wslpath -m "$(dirname "$HEX_PATH")")/
HEX_FILE=$(basename "$HEX_PATH")

# Start teensy loader if it isn't already running
echo "Starting Teensy Loader (if necessary)..."
nc -z localhost 3149 || nohup "$TEENSY_LOADER_PATH" >/dev/null 2>&1 &

# Wait until teensy loader is ready to accept commands
echo "Waiting for Teensy Loader to get ready..."
while ! nc -z localhost 3149; do :; done

# Send commands to teensy loader
echo "Instructing Teensy Loader to upload $HEX_FILE..."
echo -e "dir:$HEX_DIR\nfile:$HEX_FILE\nauto:on" | nc localhost 3149
