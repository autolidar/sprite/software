#!/usr/bin/env bash

source scripts/utils.sh --prefix "$0"

apt_install python3-pip
pip_install cmake-format
