This directory contains utility scripts for Project Sprite.

## Scripts

- [`install/`](install/): The scripts in this directory handle installation of dependencies not directly related to ROS.
  For now, the only dependencies in this category are linting/formatting tools.

  - [`install/all_lint_tools.sh`](install/all_lint_tools.sh): This script installs all the tools necessary for linting,
    by calling each of the scripts below. Each linting tool is separated into its own script, so that individual CI
    stages can be run in parallel to speed up the process. See [`.gitlab-ci.yml`](../.gitlab-ci.yml) for details on how
    this is done.

  - [`install/python_lint_tools.sh`](install/python_lint_tools.sh): This script handles installation of the linting tool
    for Python code: [flake8](https://pypi.org/project/flake8/). It also installs [black](https://pypi.org/project/black/)
    and [isort](https://pypi.org/project/isort/), and plugins to integrate them
    ([flake8-black](https://pypi.org/project/flake8-black/) and [flake8-isort](https://pypi.org/project/flake8-isort/)).

  - [`install/cmake_lint_tools.sh`](install/cmake_lint_tools.sh): This script handles installation of the linting tool
    for CMakeLists.txt files: [cmake-format](https://pypi.org/project/cmake-format/).

  - [`install/other_lint_tools.sh`](install/other_lint_tools.sh): This script handles installation of a linting tool that
    supports many different file types: [prettier](https://prettier.io/). Since prettier depends on
    [Node.js](https://nodejs.org/), it handles setup of the
    [NodeSource repositories](https://github.com/nodesource/distributions) and installation of Node.js as well.

- [`lint/`](lint/): The scripts in this directory handle running the various linting/formatting tools. By default, the
  scripts only check the code, raising errors if any issues are found. However, if `--fix` is used (e.g.
  `scripts/lint/all.sh --fix`), then the scripts will go ahead and fix any issues found in-place.

  - [`lint/all.sh`](lint/all.sh): This script runs all the linting tools, by calling each of the scripts below. Each
    linting tool is separated into its own script, so that individual CI stages can be run in parallel to speed up the
    process. See [`.gitlab-ci.yml`](../.gitlab-ci.yml) for details on how this is done.

  - [`lint/python.sh`](lint/python.sh): This script handles running the Python linting tools. By default, it runs `flake8`
    to report any errors. When run with `--fix`, it runs `black` instead, to automatically format code properly.

  - [`lint/cmake.sh`](lint/cmake.sh): This script handles running the CMakeLists.txt linting tool: `cmake-format`. It runs
    it in either the 'check' or the 'format-in-place' mode, depending on whether it is run with `--fix` or not.

  - [`lint/other.sh`](lint/other.sh): This script handles running the `prettier` linting tool. It runs it in either the
    'check' or the 'write' mode, depending on whether it is run with `--fix` or not.

- [`utils.sh`](utils.sh): Various utility functions used in the other scripts are defined in this file.
