Project Sprite's main repository.

[[_TOC_]]

# Getting Started

## Setting up your environment

1. Go through the following supported configurations, ordered by capability.
   Select the highest one that will work with your OS:

   | OS                                   | Hypervisor          | USB? |
   | ------------------------------------ | ------------------- | ---- |
   | Ubuntu 20.04                         | _none_              | Yes  |
   | Linux                                | Docker              | Yes  |
   | Windows 10 Build 19044+ / Windows 11 | WSL2 (Ubuntu 20.04) | Yes  |
   | Windows                              | Docker              | No   |
   | macOS                                | Docker              | No   |

2. Make sure the hypervisor for your selected configuration is installed.

   - For Docker: download and install from https://docker.com
   - For WSL2: run `wsl --install -d Ubuntu-20.04` with admin privileges.

3. If your hypervisor is _not_ Docker, manually install ROS Noetic.

   > The ROS wiki has a [guide](http://wiki.ros.org/noetic/Installation/Ubuntu)
   > on how to install ROS. Follow the guide from section 1.2 to 1.4. The
   > ROS-Base / `ros-noetic-ros-base` is sufficient for our purposes.

4. If your selected configuration is Windows + Docker, download and install Git
   from https://git-scm.com/download/win

   - Make sure to select "Checkout as-is, commit Unix-style line endings"
     when given the option.
   - Make sure to select "Enable symbolic links" when given the option.

5. Download and install VS Code from https://code.visualstudio.com.
   Once installed, open it. Exit the "Get Started" walkthrough if necessary.

6. If your hypervisor is WSL2, set up VS Code for WSL:

   - Launch VS Code Quick Open (press `Ctrl+P`).
   - Run `ext install ms-vscode-remote.remote-wsl`.
   - Open the Command Palette (press `Ctrl-Shift-P`).
   - Search for and run the "New WSL Window using Distro" command.
   - Select "Ubuntu-20.04". This will open a new window connected to WSL.

7. Clone the repository:

   - On the welcome page, under "Start", select "Clone Git Repository..."
   - Repository URL: https://gitlab.com/autolidar/sprite/software.git
   - Make sure to select a destination that has no spaces in its path.
   - When prompted, open the repository, then trust the folder.

8. Install the necessary extensions:

   - Open the Command Palette (press `Ctrl-Shift-P` or `Cmd-Shift-P`).
   - Search for and run the "Show Recommended Extensions" command.
   - Click the download icon next to "Workspace Recommendations".

9. If your hypervisor is Docker, reopen the project:

   - Open the Command Palette (press `Ctrl-Shift-P` or `Cmd-Shift-P`).
   - Search for and run the "Reopen in Container" command.

## Setting up the project

Open a terminal in VS Code (Terminal > New Terminal), and run `. bootstrap.sh`. This script does the following:

- Fetches, builds, and installs [CSM](https://censi.science/software/csm/), if necessary. CSM is a dependency for
  building [laser_scan_matcher](https://wiki.ros.org/laser_scan_matcher).

- Pulls in third-party dependencies that are not available in the `apt` repositories using
  [wstool](https://wiki.ros.org/wstool).

- Installs all necessary dependencies from `apt` using [rosdep](https://wiki.ros.org/rosdep).

- Runs [catkin_make](https://wiki.ros.org/catkin/commands/catkin_make) to build all the project code.

## Running the simulation

Run `roslaunch sprite simulation_gui.launch`. This will launch a [Gazebo](http://gazebosim.org/) simulation of the
vehicle, and launch the parts of the project necessary for autonomous control of the simulated vehicle.
[RViz](https://wiki.ros.org/rviz) will also be started, for data visualization.

Alternatively, run `roslaunch sprite simulation.launch`. This will do the same thing, but in headless mode (without any
visible GUI). Very useful when running on less-powerful machines. The Gazebo frontend can be started separately with
`roslaunch sprite gzclient.launch`, and RViz can be started with `roslaunch sprite rviz.launch`.

## Running on the physical vehicle

Run `roslaunch sprite main.launch`. This will launch the parts of the project necessary for autonomous control of the
physical vehicle. See the README in [src/sprite/launch/](src/sprite/launch/) for more details about the launch files.

## Setting up code linting

From the root of the repository, run `scripts/install/all_lint_tools.sh` to install everything necessary for code
linting.

To automatically format all the code, run `scripts/lint/all.sh` (again, from the root of the repository).

For more details, see the README in the [scripts/](scripts/) directory.

# Code Structure

This repository is set up as a [catkin workspace](https://wiki.ros.org/catkin/workspaces), containing individual [catkin
packages](https://wiki.ros.org/ROS/Tutorials/catkin/CreatingPackage#What_makes_up_a_catkin_Package.3F) responsible for
project functionality.

## Main files and directories

- [`bootstrap.sh`](bootstrap.sh): See [Setting up the environment](#setting-up-the-environment) for more information on
  what this script does.

- [`scripts/`](scripts/): Contains utility scripts to handle installing dependencies, formatting files, etc. See the
  README inside it for more information.

- [`src/`](src/): The [source space](https://wiki.ros.org/catkin/workspaces#Source_Space) for Project Sprite. Contains
  the catkin packages that make up the project. See the README inside it for more information.

## Tool configuration files and directories

- [`.flake8`](.flake8): Configuration file for [flake8](https://pypi.org/project/flake8/) (a tool that checks Python
  code for style and programming errors). Set up to work well with [black](https://pypi.org/project/black/) (an
  opinionated Python code formatter)

- [`.gitignore`](.gitignore): Configuration file for [git](https://git-scm.com/). Tells `git` what files to ignore when
  committing changes to the codebase. Generated using [gitignore.io](https://gitignore.io/).

- [`.gitlab-ci.yml`](.gitlab-ci.yml): Configuration file for [GitLab CI/CD](https://docs.gitlab.com/ee/ci/). Contains
  stages to ensure that code style is maintained, and that the project builds successfully. Eventually, will also ensure
  that the project works properly (see [issue 6](https://gitlab.com/autolidar/sprite/software/-/issues/6)).

- [`.isort.cfg`](.isort.cfg): Configuration file for [isort](https://pypi.org/project/isort/) (a tool that sorts imports
  in Python code). Set up to work well with `black`.

- [`.prettierignore`](.prettierignore): Configuration file for [prettier](https://prettier.io/) (an opinionated code
  formatter). Tells `prettier` what files to ignore when formatting files.

# More documentation?

Additional documentation is available in READMEs throughout the repository tree.
For team members, please also see the [Google Drive](https://tiny.cc/autolidar_drive).
