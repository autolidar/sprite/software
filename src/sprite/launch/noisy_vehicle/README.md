This directory contains the launch files for the noisy vehicle.

## First-level Launch Files

These launch files are meant to be launched using `roslaunch` (e.g. `roslaunch sprite xxxxxxx.launch`).

- [`noisy_simulation.launch`](noisy_simulation.launch): This is the launch file used to run the project on a vehicle
  with a noisy LiDAR in a simulated environment. Its structure is similar to that of
  [`simulation.launch`](src/sprite/launch/simulation.launch), using [`noisy_vehicle.launch`](noisy_vehicle.launch)
  in place of [`simulated_vehicle.launch`](src/sprite/launch/simulated_vehicle.launch).

- [`noisy_simulation_gui.launch`](noisy_simulation_gui.launch): This launch file is a simple shortcut to launch the simulation along
  with RViz and the Gazebo frontend. It simply includes [`noisy_simulation.launch`](noisy_simulation.launch),
  [`rviz.launch`](src/sprite/launch/rviz.launch), and [`gzclient.launch`](src/sprite/launch/gzclient.launch).

## Other Launch Files

These launch files are not meant to be launched directly with `roslaunch`, but rather are included from other launch
files.

- [`noisy_vehicle.launch`](noisy_vehicle.launch): This launch file is a parallel to [`simulated_vehicle.launch`](src/sprite/launch/simulated_vehicle.launch)
  and [`actual_vehicle.launch`](src/sprite/launch/actual_vehicle.launch), in that it sets up everything related to the simulated vehicle. This
  includes:

  - Loading the vehicle model using [`noisy_vehicle_urdf.launch`](noisy_vehicle_urdf.launch) (described below).

  - Starting the Gazebo simulation with a specific world (from [src/sprite/worlds/](src/sprite/worlds/)).

  - Spawning the vehicle into the simulated world, and setting up the necessary physics controllers.

- [`noisy_vehicle_urdf.launch`](noisy_vehicle_urdf.launch): This launch file loads in the vehicle model from the
  URDF file (located at [src/sprite/urdf/traxxas_6804r_noise.urdf.xacro](src/sprite/urdf/traxxas_6804r_noise.urdf.xacro)).
