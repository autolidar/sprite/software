#!/usr/bin/env python2

from math import pi
from sys import stdout

import rospy
from sensor_msgs.msg import LaserScan
from serial import Serial

SERIAL_PORT = "/dev/serial/by-id/usb-Teensyduino_USB_Serial_7014900-if00"  # Serial id for LiDAR's Teensy

SCAN_HZ = 3.5  # Encoder frequency
SCAN_CPR = 800  # Encoder counts per revolution; modified to '800' to expect channel A & B interrupts
SCAN_TRIM = 0
# 7 * pi / 6  # Adjust scan trim to set the LiDAR's reading to match true value

MAX_DISTANCE = 200  # meters


def process_ray():
    global arduino
    prev_byte = "0"

    while True:  # Runs until both a distance and angle value are processed
        curr_byte = hex(ord(arduino.read(size=1)))
        # check if the past two bytes indicate a packet sent from the LiDAR firmware
        if (
            str(prev_byte) == "0x55" and str(curr_byte) == "0xaa"
        ):  # Packet's start bytes are "0x55" and "0xaa"

            # Encoder readings
            enc_ctr_LSB = ord(arduino.read(1))
            enc_ctr_MSB = ord(arduino.read(1))

            # Distance readings
            rgf_dist_LSB = ord(arduino.read(1))
            rgf_dist_MSB = ord(arduino.read(1))

            # reconstruct the encoder count by putting together
            # Most Signficant Bytes and Least Signficant Bytes
            enc_ctr = (enc_ctr_MSB << 8) + enc_ctr_LSB
            # calculate the angle of this reading
            angle = ((float(enc_ctr) / SCAN_CPR) * 2 * pi) % (2 * pi)

            # reconstruct the distance reading by putting together
            # Most Signficant Bytes and Least Signficant Bytes
            distance = (rgf_dist_MSB << 8) + rgf_dist_LSB

            if distance == 65535:
                distance = 0
            # print("encoder count = {}, distance = {}".format(enc_ctr, distance))
            return (distance / 100.0, angle)
        else:
            prev_byte = curr_byte


def main():
    global arduino

    # Initialize ROS node
    print("Initializing ROS node...")
    rospy.init_node("sprite_scanning_lidar")

    # Create a ROS publisher to publish LiDAR scan data
    scan_pub = rospy.Publisher("scan", LaserScan, queue_size=50)

    # Connect to the host
    print("Initializing Arduino...")
    baud_rate = 3500000
    arduino = Serial(SERIAL_PORT, baudrate=baud_rate)

    print("Waiting for scan start...")

    # Read in ray(distance, angles) values from the host
    ray_prev = None
    ray_curr = process_ray()
    while True:  # Waits to start at angle 0
        ray_prev = ray_curr
        ray_curr = process_ray()
        if ray_curr[1] < ray_prev[1]:  # Revolution is complete
            break

    # Construct rangefinder's data into /scan topic
    while not rospy.is_shutdown():
        print("Scanning... "),
        stdout.flush()
        current_time = rospy.Time.now()
        rays = []
        while True:  # Waits to start at angle 0
            rays.append(ray_curr)  # append rays(distance, angle)
            ray_prev = ray_curr
            ray_curr = process_ray()
            if ray_curr[1] < ray_prev[1]:  # Revolution is complete
                break

        print("Done. Processing... "),
        stdout.flush()
        ranges = []
        angles = []

        # Parse rays into ranges and angles list
        for i in rays:
            ranges.append(i[0])
            angles.append(i[1] + SCAN_TRIM)

        scan = LaserScan()  # Create an object for /scan topic

        # Feed in data to match the /scan topic parameters
        scan.header.stamp = current_time
        scan.header.frame_id = "base_laser"
        scan.angle_min = angles[0]  # Sets minimum angle to first angle recorded
        scan.angle_max = angles[-1]  # Sets maximum angle to last angle recorded
        scan.angle_increment = (scan.angle_max - scan.angle_min) / len(rays)

        # scan.scan_time = (int(rays[-1][1]) - int(rays[1][1])) / 1000000.0

        # if no measurements are received, hardcode the values. Not ideal!
        scan.scan_time = 1 / SCAN_HZ
        scan.time_increment = scan.scan_time / len(rays)
        scan.range_min = 0.0
        scan.range_max = MAX_DISTANCE
        scan.ranges = ranges

        scan_pub.publish(scan)  # Publish scan readings

        print("Done.")


if __name__ == "__main__":
    main()
