#pragma once

#include "common.h"

class lwSerialPort {
	public:
		// Establish a serial connection with bit rate
		virtual bool connect(const char* Name, int BitRate) = 0;

		// Disconnect from the device
		virtual bool disconnect() = 0;

		// Write data to the Serial buffer given a buffersize
		virtual int writeData(uint8_t *Buffer, int32_t BufferSize) = 0;

		// Read 32-bit data from the Serial buffer given a buffersize
		virtual int32_t readData(uint8_t *Buffer, int32_t BufferSize) = 0;
};
