// Library imports
#include <Arduino.h>
#include <Servo.h>
#include "common.h"
#include "lwNx.h"

// Pin Configuration
#define PIN_ENC_IDX 6
#define PIN_ENC_CHA 7
#define PIN_ENC_CHB 8
#define PIN_ESC_PWM 5
#define SET_ESC_PWM 1925


// Global variables

/*
 state = 0: idle (waiting for encoder interrupt)
 state = 1: encoder interrupt received , waiting for rangefinder reading (receive lidar distance)
*/
volatile uint8_t state = 0;

/*
an integer between 0 and 200, representing the lidar's current angle
using 16 bits to allow for future switch to encoder disk with CPR > 255
*/
volatile uint16_t enc_ctr = 0;

Servo esc;
lwSerialPort* serial; // Create a Teensy Serial object
uint16_t distance;
uint16_t firstReturnStrength;


// System interrupts

/*
Reset encoder counter for every motor revolution
*/
void isr_enc_idx() {
    enc_ctr = 0;
}

/*
Increment encoder count for Channel A and set state to high
*/
void isr_enc_cha() {
    ++enc_ctr;
    state = 1;
}

/*
Increment encoder count for Channel B and set state to high
*/
void isr_enc_chb() {
    ++enc_ctr;
    state = 1;
}

// Helper functions

/*
Print debugging value in hex. Currently set to nothing to override _write function.
Note: Printing any additional values to Serial monitor can disrupt the flow of packets
*/
void printHexDebug(uint8_t* Data, uint32_t Size) {
	Serial.printf("");
}

/*
Blink the builtin LED to indicate a connection/command failure
*/
void exitCommandFailure(uint8_t delayMs) {
  while (1) {
    digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
    delay(delayMs);
  };
}


/*
Return 16-bit int from a 8-bit Buffer. Used to receive packets from the LiDAR unit
Arguments:
  Buffer : pointer to buffer
  Offset : index of buffer
Returns:
    result : 16 bits of data read from Buffer[Offset]
*/
uint16_t readInt16(uint8_t* Buffer, uint32_t Offset) {
	uint16_t result;
	result = (Buffer[Offset + 0] << 0) | (Buffer[Offset + 1] << 8);
	return result;
}

/*
Establish serial communication between Teensy -> Host and Teensy -> rangefinder

Setup pin connections and interrupts

Configure rangefinder to stream distance data to the Teensy
*/
void setup() {

    Serial.begin(3500000); //Begin Serial Monitor on host
    pinMode(LED_BUILTIN, OUTPUT);
    while (!Serial) {
        digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
        delay(1000);
    };


    platformInit(); // Intialize Teensy platform

    const char* portName = ""; // Portname for the Serial Connection. Left blank to satisfy the function parameters
    int32_t baudRate = 921600; // Baudrate to communicate with rangefinder

    serial = platformCreateSerialPort(); // Create a Teensy Serial port object

    // Connect to rangefinder
  	serial->connect(portName, baudRate);

    // // Read the product name. (Command 0: Product name)
  	// char modelName[16];
  	// if (!lwnxCmdReadString(serial, 0, modelName)) { exitCommandFailure(); }
    //
  	// // Read the hardware version. (Command 1: Hardware version)
  	// uint32_t hardwareVersion;
  	// if (!lwnxCmdReadUInt32(serial, 1, &hardwareVersion)) { exitCommandFailure(); }
    //
  	// // Read the firmware version. (Command 2: Firmware version)
  	// uint32_t firmwareVersion;
  	// if (!lwnxCmdReadUInt32(serial, 2, &firmwareVersion)) { exitCommandFailure(); }
  	// char firmwareVersionStr[16];
  	// lwnxConvertFirmwareVersionToStr(firmwareVersion, firmwareVersionStr);
    //
  	// // Read the serial number. (Command 3: Serial number)
  	// char serialNumber[16];
  	// if (!lwnxCmdReadString(serial, 3, serialNumber)) { exitCommandFailure(); }
    //
  	// Serial.printf("Model: %.16s\r\n", modelName);
  	// Serial.printf("Hardware: %d\r\n", hardwareVersion);
  	// Serial.printf("Firmware: %.16s (%d)\r\n", firmwareVersionStr, firmwareVersion);
  	// Serial.printf("Serial: %.16s\r\n", serialNumber);

  	// Set the output rate to 20010 readings per second. (Command 76: Update rate)
  	if (!lwnxCmdWriteUInt8(serial, 76, 0)) { exitCommandFailure(2000); }

  	// Set distance output to include distance and temperature only: (Command 29: Distance output)
  	if (!lwnxCmdWriteUInt32(serial, 29, 0b10000001)) { exitCommandFailure(4000); }

    // // Enable streaming of point data. (Command 30: Stream)
  	// if (!lwnxCmdWriteUInt32(serial, 30, 5)) { exitCommandFailure(8000); }

    // Turn on Teensy LED to indicate active communication session
    digitalWrite(LED_BUILTIN, HIGH);


    // Set up encoder pins
    pinMode(PIN_ENC_IDX, INPUT);
    pinMode(PIN_ENC_CHA, INPUT);
    pinMode(PIN_ENC_CHB, INPUT);

    // Connect Gimbal Motor ESC
    esc.attach(PIN_ESC_PWM);
    // Start scanning
    esc.writeMicroseconds(SET_ESC_PWM);

    // Interrupts are triggered when there is a change in encoder readings
    attachInterrupt(digitalPinToInterrupt(PIN_ENC_IDX), isr_enc_idx, RISING);
    attachInterrupt(digitalPinToInterrupt(PIN_ENC_CHA), isr_enc_cha, CHANGE);
    attachInterrupt(digitalPinToInterrupt(PIN_ENC_CHB), isr_enc_chb, CHANGE);

}

/*
Listen for two signals, a distance reading from the LiDAR unit or an interrupt
from the Encoder.

On receiving a distance reading packet from the LiDAR unit,
record the distance and strength of a scan.

On receiving an encoder interrupt, create a packet using the most recently
acquired distance reading and send it to the host.
*/
void loop() {
  uint8_t response[16];

  // Read data from command 44
  if (lwnxCmdReadData(serial, 44, response, 16)) {
    distance = readInt16(response, 0);
    //uint16_t temperature = readInt16(response, 2);
    // Serial.printf(
    //     "Distance: %5.2f    Temperature: %5.2f\r\n",
    //     distance / 100.0,
    //     temperature / 100.0
    // );
  }

  if (state == 1) {
    // create a packet for the host to receive and parse
    uint8_t buf[] = {
        0x55, 0xAA,                     // marks beginning of message
        (uint8_t) (enc_ctr >> 0),       // enc_ctr LSB
        (uint8_t) (enc_ctr >> 8),       // enc_ctr MSB
        (uint8_t) (distance >> 0),      // distance LSB
        (uint8_t) (distance >> 8),      // distance MSB
    };

    // Change the length if elements are added to buf[]
    Serial.write(buf, 6);               // write message to host
    state = 0;                          // start waiting for enc interrupt
  }

  delay(1);
}
