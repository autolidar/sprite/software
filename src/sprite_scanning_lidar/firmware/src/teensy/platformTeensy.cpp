#include "platformTeensy.h"
#include "lwSerialPortTeensy.h"

void platformInit() { }

int64_t platformGetMicrosecond() {
	return micros();
}

int32_t platformGetMillisecond() {
	return millis();
}

bool platformSleep(int32_t TimeMS) {
	delay(TimeMS);
	return true;
}

lwSerialPort* platformCreateSerialPort() {
	return new lwSerialPortTeensy();
}
