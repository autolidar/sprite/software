#include "lwSerialPortTeensy.h"

bool lwSerialPortTeensy::connect(const char* Name, int BitRate) {
	Serial1.begin(BitRate);
	return true;
}

bool lwSerialPortTeensy::disconnect() {
	Serial1.end();
	return true;
}

int lwSerialPortTeensy::writeData(uint8_t *Buffer, int32_t BufferSize) {
	int writtenBytes = Serial1.write(Buffer, BufferSize);

	if (writtenBytes != BufferSize) {
		return -1;
	}

	return writtenBytes;
}

int32_t lwSerialPortTeensy::readData(uint8_t *Buffer, int32_t BufferSize) {
	int readBytes = Serial1.readBytes(Buffer, BufferSize);
	return readBytes;
}
