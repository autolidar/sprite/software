function plot_laser_scan(laser_scan)

angles = laser_scan.angle_min:laser_scan.angle_increment:laser_scan.angle_max;
polarplot(laser_scan.raw_data.angle, laser_scan.raw_data.dist, '.', angles, laser_scan.ranges, ':.');

end

