
lidar_scans = split_lidar_data(augment_lidar_data(readtable('lidar_data.csv')));
for i = 1:length(lidar_scans)
    laser_scans(i,1) = convert_augmented_scan(lidar_scans{i}); %#ok<SAGROW>
    plot_laser_scan(laser_scans(i)); rlim([0 7]);
    saveas(gcf, sprintf('LaserScan%i.png', i));
end
clear i


