FROM ros:noetic-perception
COPY install /opt/sprite
RUN \
  apt-get update && \
  rosdep install -yi --from-paths /opt/sprite -t exec && \
  apt-get clean && \
  rm -rf /var/lib/apt/lists/*
